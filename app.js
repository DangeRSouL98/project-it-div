function login__validation(){
    var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var email = document.getElementById("inputted_username");
    var password = document.getElementById("inputted_password");

    if (!pattern.test(email.value)) {
        alert('invalid email')
    }else if (password.value == "" || password.value == undefined){
        alert('invalid password')
    }else {
        alert('success')
    }
}

function register__validation(){
    var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var name = document.getElementById("regName");
    var regEmail = document.getElementById("inputted_email");
    var male = document.getElementById("regGenderMale");
    var female = document.getElementById("regGenderFemale");
    var addr = document.getElementById("regAddress");
    var checkStreet = /street/;

    if (name.value == "" || name.value == undefined) {
        alert('invalid name')
    }else if (!pattern.test(regEmail.value)) {
        alert('invalid email')
    }else if (!checkStreet.test(addr.value)){
        alert('invalid address')
    }else{
        alert('success')
    }

}